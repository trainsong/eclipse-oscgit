package net.oschina.git.utils;

import java.io.IOException;

import org.eclipse.equinox.security.storage.ISecurePreferences;
import org.eclipse.equinox.security.storage.SecurePreferencesFactory;
import org.eclipse.equinox.security.storage.StorageException;
public class UserData {
	public static void saveData(String username,String password,String private_token,String cloneType){
		 ISecurePreferences preferences = SecurePreferencesFactory
                 .getDefault();
         ISecurePreferences node = preferences.node("net.osc.git");
         try {
         	 node.put("username", username, true);
             node.put("password", password, true);
             node.put("cloneType", cloneType, true);
             node.put("private_token", private_token, true);
             try {
            	 preferences.flush();
 			 } catch (IOException e) {
 				// TODO Auto-generated catch block
 				e.printStackTrace();
 			 }
         } catch (StorageException e1) {
             e1.printStackTrace();
         }
	}
	public static void removeKeyValue(String key){
		ISecurePreferences preferences = SecurePreferencesFactory
                .getDefault();
        if (preferences.nodeExists("net.osc.git")) {
            ISecurePreferences node = preferences.node("net.osc.git");
            node.remove(key);
            try {
            	preferences.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
	}
	public static String getValue(String key){
		ISecurePreferences preferences = SecurePreferencesFactory
                .getDefault();
		String value = null;
        if (preferences.nodeExists("net.osc.git")) {
            ISecurePreferences node = preferences.node("net.osc.git");
            try {
                value = node.get(key, "N/A");//如果没有匹配返回N/A
            } catch (StorageException e1) {
                e1.printStackTrace();
            }
        }
        return value;
	}
	public static void saveKeyValue(String key,String value){
		ISecurePreferences preferences = SecurePreferencesFactory
                .getDefault();
        ISecurePreferences node = preferences.node("net.osc.git");
        try {
        	node.put(key, value, true);
        	node.flush();
        } catch (StorageException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
